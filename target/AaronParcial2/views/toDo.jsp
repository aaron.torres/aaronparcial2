<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<%@ page import = "com.softtek.academy.model.BeanModel" %>

<%

String contextPath = request.getContextPath();

%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>To Do</title>
</head>
<body>
    <p>Tareas por hacer</p>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <table border="2" width="70%" cellpadding="2">
            <tr>
                <th>Actividades por hacer</th>
            </tr>
            <c:forEach var="var" items="${lista}">
	            <form action="Updatectrol" method="post">
	            <tr>
	               <td>${var.act}</td>
	               <input type="hidden" name="ActividadDone" value="${var.id}">
	               <td> <button type="submit">Done!</button> </td>
	            </tr>
	            </form>
            </c:forEach>
        </table>
        <a href="<%= contextPath %>/ControllerDone">Go to "Done" list</a>
</body>
</html>