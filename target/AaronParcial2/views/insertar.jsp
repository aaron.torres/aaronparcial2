<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%

String contextPath = request.getContextPath();

%>
	
<!DOCTYPE html>
<html>
    <head>
    	<meta charset="ISO-8859-1">
        <title>Add new activity</title>
    </head>
    <body>
        <form action="/ToDoList/AddController" method="post">
	        New activity name: <input type="text" name="act"><br>
	    	<input type="submit" name="btn_add" value="Add">
    	</form>
    	<br/>
    	<a href="<%= contextPath %>
		/ControllerServlet">Go to "To Do" list</a>
		<br/>
    </body>
</html>