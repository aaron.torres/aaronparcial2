package com.softtek.academy.views;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.softtek.academy.model.BeanModel;

public class AddDao {
	public String checkInsert(BeanModel addBean)
    {
        String act=addBean.getAct();
        
        try
        {

        	Connection con = Conexion.getConnection(); 
            
            PreparedStatement pstm=null; 
            
            pstm=con.prepareStatement("insert into TO_DO_LIST (LIST) values (?)"); 
            pstm.setString(1,act);
            pstm.executeUpdate(); 
            
            pstm.close();
            
            con.close(); 
            
            return "INSERT SUCCESS"; 
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return "FAIL INSERT"; 
        }
        
        
    }
    
}
