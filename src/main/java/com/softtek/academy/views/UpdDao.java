package com.softtek.academy.views;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.softtek.academy.model.BeanModel;

public class UpdDao {
	public String checkUpdate(BeanModel addBean)
    {
        String act=addBean.getAct();
        
        try
        {

        	Connection con = Conexion.getConnection(); 
            
            PreparedStatement pstmt=null; 
            
            pstmt=con.prepareStatement("UPDATE TO_DO_LIST set is_done=True  WHERE id=?"); 
            pstmt.setString(1,act);
            pstmt.executeUpdate(); 
            
            pstmt.close();
            
            con.close(); 
            
            return "UPDATE SUCCESS"; 
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return "FAIL UPDATE"; 
        }
        
        
    }
    

}
