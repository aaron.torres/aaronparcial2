package com.softtek.academy.views;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
private static Connection conn;
	
	private Conexion(){}
	
	public static Connection getConnection() {
        final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
        final String DB_URL = 
        		//"jdbc:mysql://127.0.0.1:3306/sesion1?serverTimezone=UTC#";
        		"jdbc:mysql://localhost:3306/sesion1?useUnicode="
        		+ "true&useJDBCCompliantTimezoneShift="
        		+ "true&useLegacyDatetimeCode="
        		+ "false&serverTimezone=UTC";
       
        final String USER = "root";
        final String PASS = "1234";
        
        conn = null;
        
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
        }catch(Exception e) {
            System.out.println(e);
        }
        return conn;
    }
}
