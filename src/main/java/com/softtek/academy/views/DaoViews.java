package com.softtek.academy.views;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.mysql.cj.protocol.Resultset;
import com.softtek.academy.model.BeanModel;

public class DaoViews {
	
	ArrayList<BeanModel> lb = new ArrayList<BeanModel>();
    
	

    public ArrayList<BeanModel> getAllAct() {
        try {
        	Connection con = Conexion.getConnection();
            
            PreparedStatement ps = con.prepareStatement("SELECT * FROM TO_DO_LIST WHERE IS_DONE = FALSE");
            ResultSet rs = ps.executeQuery();
           
            while(rs.next()) {
                BeanModel act = new BeanModel();    
                act.setAct(rs.getString("LIST"));
                act.setId(rs.getInt("ID"));
                lb.add(act);
            }
            
            ps.close();
            
            con.close(); 
           
        }catch(Exception e) {
            System.out.println(e);
            return lb;
        }
        System.out.println(lb);
        return lb;
    }
}