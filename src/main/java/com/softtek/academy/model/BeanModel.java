package com.softtek.academy.model;

public class BeanModel {
	private String act;
    private int id;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }   
    
    public String getAct() {
        return act;
    }
    public void setAct(String act) {
        this.act = act;
    }   
}
