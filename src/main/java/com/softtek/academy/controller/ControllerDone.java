package com.softtek.academy.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.views.DaoDone;

/**
 * Servlet implementation class ControllerDone
 */
public class ControllerDone extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerDone() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
    	response.setContentType("text/html");

        DaoDone ld = new DaoDone();
 
        request.setAttribute("lista", ld.getAllAct());
        request.getRequestDispatcher("/views/done.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

        DaoDone ld = new DaoDone();
 
        request.setAttribute("lista", ld.getAllAct());
        request.getRequestDispatcher("/views/done.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */


}
