package com.softtek.academy.controller;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.model.BeanModel;
import com.softtek.academy.views.UpdDao;


public class UpdateController extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateController() {
        super();
        // TODO Auto-generated constructor stub
    }

	
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    { 
    	response.setContentType("text/html");
    	
        if(request.getParameter("ActividadDone")!=null) 
        {
            String act=request.getParameter("ActividadDone"); 
            //PrintWriter out=response.getWriter();
            //out.println("Actividad a actualizar: "+act);
    
            BeanModel addBean=new BeanModel(); 
            
            addBean.setAct(act); 
            
            UpdDao addDao=new UpdDao(); 
            
            String insertValidate=addDao.checkUpdate(addBean);
            
            if(insertValidate.equals("UPDATE SUCCESS")) 
            {
            
                RequestDispatcher rd=request.getRequestDispatcher("ControllerDone");
                rd.forward(request, response);
            }
            else
            {
             
                RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
                rd.include(request, response);
            }
        }
       }
}