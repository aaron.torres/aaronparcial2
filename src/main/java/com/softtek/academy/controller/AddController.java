package com.softtek.academy.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.model.BeanModel;
import com.softtek.academy.views.AddDao;

/**
 * Servlet implementation class AddController
 */
public class AddController extends HttpServlet {

	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddController() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
    	response.setContentType("text/html");
        if(request.getParameter("btn_add")!=null) 
        {
            String act=request.getParameter("act"); 
            
            BeanModel addBean=new BeanModel(); 
            
            addBean.setAct(act); 
            
            AddDao addDao=new AddDao(); 
            
            String insertValidate=addDao.checkInsert(addBean);
            
            if(insertValidate.equals("INSERT SUCCESS")) 
            {
                RequestDispatcher rd=request.getRequestDispatcher("/views/insertar.jsp");
                rd.forward(request, response);
            }
            else
            {
                RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
                rd.include(request, response);
            }
        }
    }
}
